Source: ptux-essential
Section: admin
Priority: optional
Maintainer: Bill Gatliff <bgat@billgatliff.com>
Build-Depends: debhelper
Standards-Version: 4.3.0
Vcs-Git: https://gitlab.com/pragmatux/ptux-essential.git
Vcs-Browser: https://gitlab.com/pragmatux/ptux-essential.git

Package: ptux-essential
Architecture: all
Description: Essential system packages (metapackage)
 These are the essential packages on the system, i.e. those which any other
 package can depend on without explicitly specifying a dependency. These
 packages supply all of their core functionality even when unconfigured.
 .
 A pin at priority -1 (do not install) is added to apt_preferences(5)
 for Debian archive packages marked essential that are not desired. This
 prevents 'apt-get dist-upgrade', which installs all the essential
 packages it finds, from installing them.
 (metapackage)
Depends:
 base-files,
 base-passwd,
 bash,
 bsdutils,
 coreutils,
 dash,
 debianutils,
 diffutils,
 dpkg,
 e2fsprogs,
 findutils,
 grep,
 gzip,
 hostname,
 libc-bin,
 login,
 mount,
 ncurses-base,
 ncurses-bin,
 perl-base,
 sed,
 sysvinit-utils,
 tar,
 util-linux,
 ${misc:Depends}
